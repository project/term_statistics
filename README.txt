
README

Description
-----------

Based on the core statistics module, which tracks general page views and 
node views, term_statistics keeps track of views of categories (terms)
and includes a page and a block summarizing most-viewed categories.
Statistical collection does cause a little overhead; thus everything comes
disabled by default.

The module counts how many times each of your categories is viewed. Once 
we have that count the module can do the following with it:

* The count can be displayed in the term's page display.
* Several configurable block can be added which can display the day's top terms,
  the all time top terms, and the last terms displayed. Each block has a title,
  which you can change, as well as being able to change how many term titles
  will be displayed.

Notes on using the statistics:

If you enable the view counters for terms, this adds 1 database query for 
each term that is viewed (2 queries if it's the first time the term has 
ever been viewed).

As with any new module, the term_statistics module needs to be enabled
before you can use it. Also refer to the permissions section, as this 
module supports four separate permissions.

Popular categories block
------------------------

This module creates several block that can display the day's top viewed 
categories, the all time top viewed categories, and the last categories 
viewed. Each of these blocks can be enabled or disabled individually, and
the number of categories displayed for each can be configured with a drop
down menu.

Don't forget to enable the blocks.

Popular categories page
-----------------------

This module creates a user page that can display summaries of the day's 
most popular viewed categories, the all time most popular categories, and 
the last categories viewed. Each of these summaries can be enabled or 
disabled individually, and the number of cagegories displayed for each can
be configured with a drop down menu. You can also assign a name for the 
automatically generated link to the user page. If no name is set, the 
link will not be displayed.

Permissions
------------

This module has four permissions that need to be configured in the 
permissions section.

access category statistics - enable for user roles that get to see view 
  counts for individual categories (this does not define access to the
  block)
administer category statistics module - enable for user roles that get to
  configure the statistics module.
administer category statistics - enable for user roles that get to view 
  the referrer statistics.
